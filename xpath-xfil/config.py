import string

URL = "http://ok/ok.php"

XPATH_KEY = """contains(Email, "ok")"""
XPATH_FIELD = "Password"

SUCCESS_TEXT = "ok@ok.com"

PROXY = {
    "http": "http://10.0.0.1:3128",
}
ALPHABET = string.ascii_letters + string.digits + string.punctuation
